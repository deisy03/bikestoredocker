package com.ADSI1836648.Bike.repository;

import com.ADSI1836648.Bike.domain.DetailUser;
import org.springframework.data.repository.CrudRepository;

public interface DetailUSerRepository extends CrudRepository<DetailUser, Long> {
}
