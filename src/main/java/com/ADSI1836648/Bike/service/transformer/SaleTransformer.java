package com.ADSI1836648.Bike.service.transformer;

import com.ADSI1836648.Bike.domain.Bike;
import com.ADSI1836648.Bike.domain.Client;
import com.ADSI1836648.Bike.domain.Sale;
import com.ADSI1836648.Bike.service.dto.ClientWithSaleDTO;
import com.ADSI1836648.Bike.service.dto.SaleDTO;

import java.time.LocalDateTime;

public class SaleTransformer {

    public static Sale getSaleFromSaleDTO(SaleDTO dto){
        if(dto == null){
            return null;
        }

        Sale sale = new Sale();
        sale.setDateSale(LocalDateTime.now());
        sale.setBike(dto.getBike());
        sale.setClient(dto.getClient());
        sale.setId(dto.getId());

        return sale;
    }

    public static SaleDTO getSaleDTOFromSale(Sale sale){
        if(sale == null){
            return null;
        }

        SaleDTO dto = new SaleDTO();
        dto.setBike(sale.getBike());
        dto.setClient(sale.getClient());
        dto.setId(sale.getId());
        dto.setDateSale(sale.getDateSale());
        return dto;
    }

    public static Sale getSaleFromClientWithSaleDTO(ClientWithSaleDTO clientWithSaleDTO) {
        if(clientWithSaleDTO == null){
            return null;
        }

        Sale sale = new Sale();
        Bike bike = new Bike();
        Client client = new Client();
        bike.setId(clientWithSaleDTO.getBikeId());
        bike.setStatus(clientWithSaleDTO.getStatus());
        bike.setModel(clientWithSaleDTO.getModel());
        bike.setSerial(clientWithSaleDTO.getSerial());
        bike.setPrice(clientWithSaleDTO.getPrice());

        client.setDocumentNumber(clientWithSaleDTO.getDocumentNumber());
        client.setEmail(clientWithSaleDTO.getEmail());
        client.setName(clientWithSaleDTO.getName());
        client.setPhoneNumber(clientWithSaleDTO.getPhoneNumber());
        client.setId(clientWithSaleDTO.getClientId());

        sale.setId(clientWithSaleDTO.getId());
        sale.setDateSale(clientWithSaleDTO.getDateSale());
        sale.setBike(bike);
        sale.setClient(client);
        return sale;
    }

    public static ClientWithSaleDTO getSaleDTOFromClientWithSaleDTO(Sale sale) {
        if(sale == null){
            return null;
        }

        ClientWithSaleDTO clientWithSaleDTO  = new ClientWithSaleDTO();

        clientWithSaleDTO.setBikeId(sale.getBike().getId());
        clientWithSaleDTO.setModel(sale.getBike().getModel());
        clientWithSaleDTO.setSerial(sale.getBike().getSerial());
        clientWithSaleDTO.setPrice(sale.getBike().getPrice());
        clientWithSaleDTO.setStatus(sale.getBike().getStatus());
        clientWithSaleDTO.setDocumentNumber(sale.getClient().getDocumentNumber());
        clientWithSaleDTO.setName(sale.getClient().getName());
        clientWithSaleDTO.setEmail(sale.getClient().getEmail());
        clientWithSaleDTO.setClientId(sale.getClient().getId());
        clientWithSaleDTO.setPhoneNumber(sale.getClient().getPhoneNumber());
        return clientWithSaleDTO;
    }
}
